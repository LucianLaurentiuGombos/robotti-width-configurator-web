let canvas = document.getElementById('robottiCanvas');
let canvasContainer = document.getElementById('canvasContainer');
let newCanvasWidth;
let newCanvasHeight;
let ctx = canvas.getContext('2d');

let leftWheelObject;
let rightWheelObject;
let globalCropWidth = 0;
let allCrops = []
let leftCrops_from_leftWheel = [];
let rightCrops_from_leftWheel = [];
let cropsInside_leftHalf_of_leftWheel = [];
let cropsInside_rightHalf_of_leftWheel = [];
let cropsBetweenWheels = [];



//Inputs
const implementWidthInput = document.getElementById('implementWidth');
const cropWidthInput = document.getElementById('cropWidth');
const rowDistanceInput = document.getElementById('rowDistance');

//Range wheel center distance
const wheelCenterDistanceInput = document.getElementById('wheelCenterDistance');

//Radio buttons
const robotti_width_165 = document.getElementById('robotti_165');
const robotti_width_365 = document.getElementById('robotti_365');

const wheelWidth_26 = document.getElementById('robotti_wheel_26');
const wheelWidth_32 = document.getElementById('robotti_wheel_32');

const centerCrop = document.getElementById('centerCrop_true');
const decenterCrop = document.getElementById('centerCrop_false');

//Inner and outer clearance for both wheels
const leftWheelOuterClearance = document.getElementById('leftWheelOuterClearance');
const leftWheelInnerClearance = document.getElementById('leftWheelInnerClearance');
//const rightWheelOuterClearance = document.getElementById('rightWheelOuterClearance');
//const rightWheelInnerClearance = document.getElementById('rightWheelInnerClearance');

//Scale in cm
const fullScale = 600;
const halfScale = 300;

//Events for changing robotti wheel center distance

wheelCenterDistanceInput.addEventListener('input', drawAll);

//Events for changing robotti wheel width
wheelWidth_26.addEventListener('change', drawAll);
wheelWidth_32.addEventListener('change', drawAll);

//Events for changing robotti implement width
implementWidthInput.addEventListener('input', drawAll);

//Event for changing crop width
cropWidthInput.addEventListener('input', drawAll);

//Event for changing crop distance
rowDistanceInput.addEventListener('input', drawAll);

//Event for centering crop
centerCrop.addEventListener('change', drawAll);
decenterCrop.addEventListener('change', drawAll);

window.addEventListener('resize', drawAll);


function drawAll(event) {

    canvas.width = canvasContainer.offsetWidth;
    canvas.height = canvasContainer.offsetWidth / 2.5;

    newCanvasWidth = canvasContainer.offsetWidth;
    newCanvasHeight = canvasContainer.offsetWidth / 2.5;

    if (canvasContainer.offsetWidth < 472) {
        canvas.height = canvasContainer.offsetWidth / 2;
        newCanvasHeight = canvasContainer.offsetWidth / 2;
    }

    //Clear canvas
    ctx.clearRect(0, 0, canvas.width, canvas.height);

    //Call main functions
    drawRuler();
    drawWheels(event);

}

function drawCrop() {

    //Clear crops arrays
    allCrops = []
    leftCrops_from_leftWheel = [];
    rightCrops_from_leftWheel = [];
    cropsInside_leftHalf_of_leftWheel = [];
    cropsInside_rightHalf_of_leftWheel = [];
    cropsBetweenWheels = [];


    let cropWidth_cm = cropWidthInput.value;
    let rowDistance_cm = rowDistanceInput.value;
    let isCropCentered = centerCrop.checked;

    let cropWidth_pixels = convertCanvas_width_CmToPixels(cropWidth_cm)
    let cropHeight = newCanvasHeight / 15;
    let crop_pos_y = newCanvasHeight - (cropHeight + 40);
    globalCropWidth = cropWidth_pixels;
    if (rowDistance_cm <= 0) {
        let crop_pos_x = isCropCentered ? (newCanvasWidth / 2) - (cropWidth_pixels / 2) : (newCanvasWidth / 2) - cropWidth_pixels;
        let crop = new Rectangle(crop_pos_x, crop_pos_y, cropWidth_pixels, cropHeight, "#8bc34a");
        crop.generate();

    } else {
        let rowDistance_percentege = (rowDistance_cm / fullScale) * 100;
        let rowDistance_pixels = (rowDistance_percentege / 100) * newCanvasWidth;

        let benchmarkCrop;
        if (isCropCentered) {
            let benchmarkCrop_pox_x = (newCanvasWidth / 2) - (cropWidth_pixels / 2)
            benchmarkCrop = new Rectangle(benchmarkCrop_pox_x, crop_pos_y, cropWidth_pixels, cropHeight, "#8bc34a");
            benchmarkCrop.generate();
            allCrops.push(benchmarkCrop);
            //Generating left side crop
            for (let i = 0; i < 300; i++) {
                let newCrop_pos_x = benchmarkCrop.pos_x - (i * rowDistance_pixels)
                let leftCrop = new Rectangle(newCrop_pos_x, crop_pos_y, cropWidth_pixels, cropHeight, "#8bc34a");
                leftCrop.generate();
                allCrops.push(leftCrop);
            }
            //Generating right side crop
            for (let i = 0; i < 300; i++) {
                let newCrop_pos_x = benchmarkCrop.pos_x + (i * rowDistance_pixels)
                let rightCrop = new Rectangle(newCrop_pos_x, crop_pos_y, cropWidth_pixels, cropHeight, "#8bc34a");
                rightCrop.generate();
                allCrops.push(rightCrop);
            }
        } else {
            let benchmarkCrop_pox_x = (newCanvasWidth / 2) - (rowDistance_pixels / 2) - (cropWidth_pixels / 2)
            benchmarkCrop = new Rectangle(benchmarkCrop_pox_x, crop_pos_y, cropWidth_pixels, cropHeight, "#8bc34a");
            benchmarkCrop.generate();

            //Generating left side crop
            for (let i = 0; i < 300; i++) {
                let newCrop_pos_x = benchmarkCrop.pos_x - (i * rowDistance_pixels)
                let leftCrop = new Rectangle(newCrop_pos_x, crop_pos_y, cropWidth_pixels, cropHeight, "#8bc34a");
                leftCrop.generate();
                allCrops.push(leftCrop);
            }
            //Generating right side crop
            for (let i = 0; i < 300; i++) {
                let newCrop_pos_x = benchmarkCrop.pos_x + (i * rowDistance_pixels)
                let rightCrop = new Rectangle(newCrop_pos_x, crop_pos_y, cropWidth_pixels, cropHeight, "#8bc34a");
                rightCrop.generate();
                allCrops.push(rightCrop);
            }
        }
    }

    populateArraysOfCrops(cropWidth_pixels)

}

function populateArraysOfCrops(cropWidth) {

    for (let i = 0; i < allCrops.length; i++) {

        let wheelCenter = leftWheelObject.pos_x + leftWheelObject.width / 2;
        let cropCenter = allCrops[i].pos_x + (allCrops[i].width / 2);
        //Collect all crops position from left wheel outer clereance
        if ((allCrops[i].pos_x + allCrops[i].width) < leftWheelObject.pos_x) {
            leftCrops_from_leftWheel.push(allCrops[i].pos_x);
        }
        //Collect all crops overlaping left half of left wheel
        if ((allCrops[i].pos_x + allCrops[i].width) > leftWheelObject.pos_x && (allCrops[i].pos_x + allCrops[i].width / 2) <= wheelCenter) {
            cropsInside_leftHalf_of_leftWheel.push(allCrops[i])
        }
        //Collect all crops overlaping right half of left wheel
        if (allCrops[i].pos_x < leftWheelObject.pos_x + leftWheelObject.width && (allCrops[i].pos_x + allCrops[i].width / 2) > wheelCenter) {
            cropsInside_rightHalf_of_leftWheel.push(allCrops[i])
        }
        //Collect all crops position from right of left wheel
        if (allCrops[i].pos_x > (leftWheelObject.pos_x + leftWheelObject.width)) {
            rightCrops_from_leftWheel.push(allCrops[i].pos_x);
        }
        //Collect crops between wheels
        if(allCrops[i].pos_x > leftWheelObject.pos_x +leftWheelObject.width && allCrops[i].pos_x+allCrops[i].width < rightWheelObject.pos_x) {
            cropsBetweenWheels.push(allCrops[i])
        }

    }

    let outerClearence_connector = findClosest(leftCrops_from_leftWheel, leftWheelObject.pos_x);
    let innerClearence_connector = findClosest(rightCrops_from_leftWheel, (leftWheelObject.pos_x + leftWheelObject.width));


    let conectorHeight = newCanvasHeight / 15;
    let conector_pos_y = newCanvasHeight - (conectorHeight + 40);

    let leftConectorLeftWheel = new Rectangle(outerClearence_connector + cropWidth, conector_pos_y, leftWheelObject.pos_x - (outerClearence_connector + cropWidth), conectorHeight, "#4fe28a85");
    let rightConectorLeftWheel = new Rectangle(leftWheelObject.pos_x + leftWheelObject.width, conector_pos_y, innerClearence_connector - (leftWheelObject.pos_x + leftWheelObject.width), conectorHeight, "#4fe28a85");

    //Do not generate green conector if crop is overlaping with left part of left wheel
    if (!cropsInside_leftHalf_of_leftWheel.length) leftConectorLeftWheel.generate()

    if ((rightConectorLeftWheel.pos_x + rightConectorLeftWheel.width) < rightWheelObject.pos_x && !cropsInside_rightHalf_of_leftWheel.length) {
        rightConectorLeftWheel.generate()
    }


    populateInnerOuterClearanceInputs(leftConectorLeftWheel, rightConectorLeftWheel);

}
function populateInnerOuterClearanceInputs(leftWheel_outer_crop, leftWheel_inner_crop) {

    let outerClearenceValue_px;
    let outerClearenceValue_cm;
    if (cropsInside_leftHalf_of_leftWheel.length) {
        let cropWidth = cropsInside_leftHalf_of_leftWheel[cropsInside_leftHalf_of_leftWheel.length - 1].width;
        outerClearenceValue_px = (cropsInside_leftHalf_of_leftWheel[cropsInside_leftHalf_of_leftWheel.length - 1].pos_x + cropWidth) - leftWheelObject.pos_x;

        outerClearenceValue_cm = convertCanvas_width_PixelsToCm(outerClearenceValue_px);
        outerClearenceValue_cm *= -1;
        //Populating inputs for outter clearance
        leftWheelOuterClearance.value = outerClearenceValue_cm;

    }  else {
        //Populating inputs for outter clearance
        leftWheelOuterClearance.value = convertCanvas_width_PixelsToCm(leftWheel_outer_crop.width);
    }

    let innerClearenceValue_px;
    let innerClearenceValue_cm;
    if (cropsInside_rightHalf_of_leftWheel.length) {

        innerClearenceValue_px = (leftWheelObject.pos_x + leftWheelObject.width) - cropsInside_rightHalf_of_leftWheel[0].pos_x;
        innerClearenceValue_cm = convertCanvas_width_PixelsToCm(innerClearenceValue_px);
        innerClearenceValue_cm *= -1;
        //Populating inputs for inner clearance
        leftWheelInnerClearance.value = innerClearenceValue_cm;
    } else if(!cropsBetweenWheels.length) {
        leftWheelInnerClearance.value = "NaN"
    }else {
        //Populating inputs for inner clearance
        leftWheelInnerClearance.value = convertCanvas_width_PixelsToCm(leftWheel_inner_crop.width);
    }


    //Displaying clearence numbers on canvas
    let fontSize;
    newCanvasWidth < 400 ? fontSize = "11px Arial" : fontSize = "13px Arial"
    ctx.font = fontSize;
    ctx.fillStyle = "black";
    ctx.fillText(leftWheelOuterClearance.value, leftWheelObject.pos_x - (leftWheelObject.width * 1.5), leftWheelObject.pos_y + (leftWheelObject.height / 1.3))
    ctx.fillText(leftWheelInnerClearance.value, leftWheelObject.pos_x + (leftWheelObject.width * 2), leftWheelObject.pos_y + (leftWheelObject.height / 1.3));

}
function convertCanvas_width_PixelsToCm(canvasObject_width) {
    let canvasObject_width_percentege = (canvasObject_width / newCanvasWidth) * 100;
    let canvasObject_width_cm = (canvasObject_width_percentege / 100) * fullScale;
    return Number((canvasObject_width_cm).toFixed(1));
}

function drawHullConnector(leftHull, rightHull) {
    let greyConector_pos_x = leftHull.pos_x + leftHull.width;
    let greyConector_pos_y = rightHull.pos_y + (rightHull.height / 1.885);
    let greyConnector_width = (rightHull.pos_x + 10) - ((leftHull.pos_x + leftHull.width) - 10)
    let greyConector_height = leftHull.height / 10.325;
    let greyConector = new ImageComponent(greyConector_pos_x, greyConector_pos_y, greyConnector_width, greyConector_height, 'images/hull-conector.png');
    greyConector.generate();

}
function drawHull(leftWheelObj, rightWheelObj, event) {

    //Variables for hull objects
    let sideHullsWidth = canvas.width / 4.71;
    let sideHullsHeight = leftWheelObj.height * 2.2;

    let leftHull_pos_x = ((leftWheelObj.pos_x) + (leftWheelObj.width / 2) - (canvas.width / 11.45));
    let leftHull_pos_y = leftWheelObj.pos_y - (leftWheelObj.height * 1.553);

    let rightHull_pos_x = ((rightWheelObj.pos_x) + (rightWheelObj.width / 2) - (canvas.width / 8));
    let rightHull_pos_y = rightWheelObj.pos_y - (rightWheelObj.height * 1.55);

    let midHullwidth = canvas.width / 4.71;
    let midHull_height = rightWheelObj.height * 1.843;
    let midHull_pos_x = (canvas.width / 2) - (midHullwidth / 2);
    let midHull_pos_y = rightWheelObj.pos_y - (rightWheelObj.height * 1.575);

    //Create hull objects
    let leftHull = new ImageComponent(leftHull_pos_x, leftHull_pos_y, sideHullsWidth, sideHullsHeight, 'images/left-hull.png');
    let rightHull = new ImageComponent(rightHull_pos_x, rightHull_pos_y, sideHullsWidth, sideHullsHeight, 'images/right-hull.png');
    let midHull = new ImageComponent(midHull_pos_x, midHull_pos_y, midHullwidth, midHull_height, 'images/mid-hull.png');

    drawImplement(midHull, event);
    //Generate hulls on canvas
    leftHull.generate();
    rightHull.generate();
    drawHullConnector(leftHull, rightHull);
    midHull.generate();
}

function drawImplement(midHull, event) {
    let imlementWidth_cm;
    if (typeof event !== "undefined" && event.target.id === "implementWidth") {
        imlementWidth_cm = event.target.value;
    } else {
        imlementWidth_cm = implementWidthInput.value
    }

    let implementWidth_pixels = convertCanvas_width_CmToPixels(imlementWidth_cm)
    let implementHeight_pixels = midHull.height / 6
    let implement_pos_x = (canvas.width / 2) - (implementWidth_pixels / 2);
    let implement_pos_y = midHull.pos_y + (midHull.height)

    let implement = new Rectangle(implement_pos_x, implement_pos_y, implementWidth_pixels, implementHeight_pixels, '#4e4a43');
    implement.generate()
}
function convertCanvas_width_CmToPixels(canvasObject) {
    let canvasObject_percentage = (canvasObject / fullScale) * 100;
    let canvasObject_pixels = (canvasObject_percentage / 100) * canvas.width;
    return canvasObject_pixels;
}

function drawWheels(event) {
    let canvasWidth = canvas.width;
    let canvasHeight = canvas.height;

    //Calculate wheel-center distance
    let wheelCenterDistance_cm;

    if (typeof event !== "undefined" && event.target.id === "wheelCenterDistance") {
        wheelCenterDistance_cm = event.target.value;
    } else {
        wheelCenterDistance_cm = wheelCenterDistanceInput.value;
    }

    //Handle invalid width
    let wheelCenterTitle = document.getElementById('centreDistanceTitle');
    let warningTitleSpan = document.createElement('span');
    wheelCenterTitle.innerHTML = 'Wheel-center distance'
    warningTitleSpan.style.color = "red";
    warningTitleSpan.style.fontWeight = "bold"
    warningTitleSpan.innerHTML = "* 165cm - 365cm";
    if (wheelCenterDistance_cm < 165) {
        wheelCenterDistance_cm = 165;
        wheelCenterTitle.appendChild(warningTitleSpan);
    } else if (wheelCenterDistance_cm > 365) {
        wheelCenterDistance_cm = 365;
        wheelCenterTitle.appendChild(warningTitleSpan);
    }

    let middleOfScale = canvasWidth / 2;
    let wheelCenterDistance_percentege = ((wheelCenterDistance_cm / 2) / halfScale) * 100;
    let wheelCenterDistance_pixels = (wheelCenterDistance_percentege / 100) * middleOfScale;
    //Calculate wheel width
    let wheelWidth_cm;

    if (typeof event !== "undefined" && event.target.id === "robotti_wheel_26") {
        wheelWidth_cm = event.target.value;
    } else if (wheelWidth_26.checked) {
        wheelWidth_cm = wheelWidth_26.value;
    }
    if (typeof event !== "undefined" && event.target.id === "robotti_wheel_32") {
        wheelWidth_cm = event.target.value;
    } else if (wheelWidth_32.checked) {
        wheelWidth_cm = wheelWidth_32.value;
    }

    let wheelWidth_percentege = (wheelWidth_cm / fullScale) * 100;
    let wheelWidth_pixels = (wheelWidth_percentege / 100) * canvasWidth;
    //Calculate wheel height
    let wheelHeight_pixels = canvasWidth / 8;

    //Variables for wheel objects
    let leftWheel_pos_x = middleOfScale - (wheelCenterDistance_pixels + (wheelWidth_pixels / 2));
    let rightWheel_pos_x = middleOfScale + (wheelCenterDistance_pixels - (wheelWidth_pixels / 2));
    let wheels_pos_y = canvasHeight - (wheelHeight_pixels + 40);

    //Create wheels objects
    let leftWheel = new ImageComponent(leftWheel_pos_x, wheels_pos_y, wheelWidth_pixels, wheelHeight_pixels, 'images/wheel.png');
    let rightWheel = new ImageComponent(rightWheel_pos_x, wheels_pos_y, wheelWidth_pixels, wheelHeight_pixels, 'images/wheel.png');

    //used for crop distance measurements
    leftWheelObject = leftWheel;
    rightWheelObject = rightWheel;

    //Draw wheels
    leftWheel.generate();
    rightWheel.generate();
    drawHull(leftWheel, rightWheel, event);
    drawWheelIndicator(leftWheel, rightWheel)

}
function drawWheelIndicator(leftWheel, rightWheel) {
    let tickWidth = 1;
    let leftWheelTick = new Line(leftWheel.pos_x + leftWheel.width / 2, newCanvasHeight - 40, leftWheel.pos_x + leftWheel.width / 2, newCanvasHeight - 30, tickWidth);
    let rightWheelTick = new Line(rightWheel.pos_x + rightWheel.width / 2, newCanvasHeight - 40, rightWheel.pos_x + rightWheel.width / 2, newCanvasHeight - 30, tickWidth);
    leftWheelTick.generate();
    rightWheelTick.generate();

    let wheelCenterPosition_width_percentege = ((rightWheel.pos_x + rightWheel.width / 2) / (newCanvasWidth)) * 100;
    wheelCenterPosition_cm = ((wheelCenterPosition_width_percentege / 100) * fullScale) - 300;
    let fontSize;
    let rightWheel_tick_pos_x;
    let leftWheel_tick_pos_x;
    (wheelCenterPosition_cm < 99) ? rightWheel_tick_pos_x = rightWheel.pos_x + (rightWheel.width / 2) - 5 : rightWheel_tick_pos_x = rightWheel.pos_x + (rightWheel.width / 2) - 10;
    (wheelCenterPosition_cm < 99) ? leftWheel_tick_pos_x = leftWheel.pos_x + (leftWheel.width / 2) - 10 : leftWheel_tick_pos_x = leftWheel.pos_x + (leftWheel.width / 2) - 15
    newCanvasWidth < 400 ? fontSize = "11px Arial" : fontSize = "13px Arial"
    ctx.font = fontSize;
    ctx.fillText(Number((wheelCenterPosition_cm).toFixed(1)), rightWheel_tick_pos_x, newCanvasHeight - 19);
    ctx.fillText(Number((wheelCenterPosition_cm * -1).toFixed(1)), leftWheel_tick_pos_x, newCanvasHeight - 19);


}
function drawRuler() {
    //Ruler line
    ctx.beginPath();
    ctx.moveTo(0, newCanvasHeight - 40);
    ctx.lineTo(canvasContainer.offsetWidth, newCanvasHeight - 40);
    ctx.stroke();

    //Ruler metric lines
    let metricLine_distance = canvasContainer.offsetWidth / 6;
    for (let i = 0; i < 7; i++) {
        ctx.beginPath();
        ctx.moveTo(metricLine_distance * i, newCanvasHeight - 40);
        ctx.lineTo(metricLine_distance * i, newCanvasHeight - 30);
        ctx.lineWidth = 2;
        ctx.stroke();

        //Add cm to metric lines
        let fontSize;
        newCanvasWidth < 400 ? fontSize = "11px Arial" : fontSize = "16px Arial"
        ctx.font = fontSize;
        switch (i) {
            case 0:
                ctx.fillText("-300", -1, newCanvasHeight - 5);
                break;
            case 1:
                ctx.fillText("-200", (metricLine_distance * i) - (newCanvasWidth < 400 ? 15 : 20), newCanvasHeight - 5);
                break;
            case 2:
                ctx.fillText("-100", (metricLine_distance * i) - (newCanvasWidth < 400 ? 15 : 20), newCanvasHeight - 5);
                break;
            case 3:
                ctx.fillText("0", (metricLine_distance * i) - (newCanvasWidth < 400 ? 3 : 4), newCanvasHeight - 5);
                break;
            case 4:
                ctx.fillText("100", (metricLine_distance * i) - (newCanvasWidth < 400 ? 10 : 15), newCanvasHeight - 5);
                break;
            case 5:
                ctx.fillText("200", (metricLine_distance * i) - (newCanvasWidth < 400 ? 10 : 15), newCanvasHeight - 5);
                break;
            case 6:
                ctx.fillText("300", (metricLine_distance * i) - (newCanvasWidth < 400 ? 20 : 26), newCanvasHeight - 5);
                break;
            default:
            // code block
        }
    }
}
function notifyToDrawCrop(img) {
    if (img == 'images/mid-hull.png') drawCrop();
}

function findClosest(arr, num) {
    if (arr == null) {
        return
    }
    return arr.sort((a, b) => Math.abs(b - num) - Math.abs(a - num)).pop();
}

class ImageComponent {
    constructor(pos_x, pos_y, width, height, src) {
        this.pos_x = pos_x;
        this.pos_y = pos_y;
        this.width = width;
        this.height = height;
        this.src = src;
    }
    generate() {
        let img = new Image();
        img.src = this.src;
        img.onload = function () {
            ctx.drawImage(img, this.pos_x, this.pos_y, this.width, this.height);
            notifyToDrawCrop(this.src);
        }.bind(this)

    }
}


class Rectangle {
    constructor(pos_x, pos_y, width, height, color) {
        this.pos_x = pos_x;
        this.pos_y = pos_y;
        this.width = width;
        this.height = height;
        this.color = color;
    }
    generate() {
        ctx.beginPath();
        ctx.fillStyle = this.color;
        ctx.fillRect(this.pos_x, this.pos_y, this.width, this.height);
        ctx.stroke();
    }
}

class Line {
    constructor(startPoint_x, startPoint_y, endPoint_x, endPoint_y, width) {
        this.startPoint_x = startPoint_x;
        this.startPoint_y = startPoint_y;
        this.endPoint_x = endPoint_x;
        this.endPoint_y = endPoint_y;
        this.width = width;
    }
    generate() {
        ctx.beginPath();
        ctx.moveTo(this.startPoint_x, this.startPoint_y);
        ctx.lineTo(this.endPoint_x, this.endPoint_y);
        ctx.lineWidth = this.width;
        ctx.stroke();
    }
}

drawAll();

